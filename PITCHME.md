#### The Gilded Rose

@ul

- Regeln
- Ausnahmen
- Link [Github](https://github.com/emilybache/GildedRose-Refactoring-Kata) ...in vielen Sprachen

@ulend

@snap[south span-100]
---?code=src/Main.elm&lang=elm
@snapend

@snap[north-east span-100 text-08 text-gray]
Elm Types
@snapend

@[1](Opaque Type !)
@[1-2]
@[4-5]